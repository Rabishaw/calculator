import java.util.Stack;

public class Postfix
{
    private String expr;
    
    public Postfix(String e)
    {
        expr = e;
    }
    
    public double eval()
    {
        String[] tokens = expr.split(" ");
        Stack<Double> s = new Stack<Double>();
        Stack<Double> abstack = new Stack<Double>();
        boolean abs = false;
        
        if (tokens.length == 1)
            return Double.parseDouble(tokens[0]);
        
        for (String token : tokens)
        {
            double x, y;
            switch(token)
            {
                case "|":
                    if(abs)
                        {
                            s.push(Math.abs(abstack.pop()));
                            abstack.clear();
                        }
                    abs = !abs;
                    break;
                case "+":
                    if(abs)
                    {
                        y = abstack.pop();
                        x = abstack.pop();
                        abstack.push(x+y);
                    }
                    else
                    {
                        y = s.pop();
                        x = s.pop();
                        s.push(x+y);
                    }
                    break;
                case "*":
                    if(abs)
                    {
                        y = abstack.pop();
                        x = abstack.pop();
                        abstack.push(x*y);
                    }
                    else
                    {
                        y = s.pop();
                        x = s.pop();
                        s.push(x*y);
                    }
                    break;
                case "-":
                    if(abs)
                    {
                        y = abstack.pop();
                        x = abstack.pop();
                        abstack.push(x-y);
                    }
                    else
                    {
                        y = s.pop();
                        x = s.pop();
                        s.push(x-y);
                    }
                    break;
                case "/":
                    if(abs)
                    {
                        y = abstack.pop();
                        x = abstack.pop();
                        abstack.push(x/y);
                    }
                    else
                    {
                        y = s.pop();
                        x = s.pop();
                        s.push(x/y);
                    }
                    break;
                case "^":
                    if(abs)
                    {
                        y = abstack.pop();
                        x = abstack.pop();
                        abstack.push(Math.pow(x,y));
                    }
                    else
                    {
                        y = s.pop();
                        x = s.pop();
                        s.push(Math.pow(x,y));
                    }
                    break;
                case "sqrt":
                    if(abs)
                    {
                        x = abstack.pop();
                        s.push(Math.sqrt(x));
                    }
                    else
                    {
                        x = s.pop();
                        s.push(Math.sqrt(x));
                    }
                    break;
                case "%":
                    if(abs)
                    {
                        y = abstack.pop();
                        x = abstack.pop();
                        abstack.push(x%y);
                    }
                    else
                    {
                        y = s.pop();
                        x = s.pop();
                        s.push(x%y);
                    }
                    break;
                case "!":
                    if(abs)
                    {
                        x = abstack.pop();
                        abstack.push(factorial(x));
                    }
                    else
                    {
                        x = s.pop();
                        s.push(factorial(x));
                    }
                    break;
                case "sin":
                    if(abs)
                    {
                        x = abstack.pop();
                        abstack.push(Math.sin(x));
                    }
                    else
                    {
                        x = s.pop();
                        s.push(Math.sin(x));
                    }
                    break;
                case "cos":
                    if(abs)
                    {
                        x = abstack.pop();
                        abstack.push(Math.cos(x));
                    }
                    else
                    {
                        x = s.pop();
                        s.push(Math.cos(x));
                    }
                    break;
                case "tan":
                    if(abs)
                    {
                        x = abstack.pop();
                        abstack.push(Math.tan(x));
                    }
                    else
                    {
                        x = s.pop();
                        s.push(Math.tan(x));
                    }
                    break;
                case "log":
                    if(abs)
                    {
                        x = abstack.pop();
                        abstack.push(Math.log10(x));
                    }
                    else
                    {
                        x = s.pop();
                        s.push(Math.log10(x));
                    }
                    break;
                case "ln":
                    if(abs)
                    {
                        x = abstack.pop();
                        abstack.push(Math.log(x));
                    }
                    else
                    {
                        x = s.pop();
                        s.push(Math.log(x));
                    }
                    break;
                default:
                    if(abs)
                        abstack.push(Double.parseDouble(token));
                    else
                        s.push(Double.parseDouble(token));
                    break;  
            }
        }
        return s.pop();
    }
    
    public double factorial(double x)
    {
        if (x == 0)
            return 1;
        else
            return x * factorial(x-1);
    }
    
    public String toString()
    {
        String toReturn = "";
        String[] tokens = expr.split(" ");
        
        for(String token : tokens)
        {
            toReturn += token;
        }
        
        return toReturn;
    }
}