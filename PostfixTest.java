

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class PostfixTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class PostfixTest
{
    /**
     * Default constructor for test class PostfixTest
     */
    public PostfixTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }
    
    @Test
    public void testEvalPlusTimes()
    {
        Postfix p = new Postfix("3 5 +");
        assertEquals(8.0, p.eval(), 0.001);
        Postfix p2 = new Postfix("3 5 + 2 *");
        assertEquals(16.0, p2.eval(), 0.001);
        Postfix p3 = new Postfix("4");
        assertEquals(4.0, p3.eval(), 0.001);
        Postfix p4 = new Postfix("1 2 3 4 * * 5 + +");
        assertEquals(30.0, p4.eval(), 0.001);
        Postfix p5 = new Postfix("-2 0.5 * 10 + 0.333333 *");
        assertEquals(3.0, p5.eval(), 0.001);
    }
    
    @Test
    public void testDivide()
    {
        Postfix p1 = new Postfix("10 2 /");
        assertEquals(5.0, p1.eval(), 0.001);
        Postfix p2 = new Postfix("2 10 /");
        assertEquals(0.2, p2.eval(), 0.001);
    }
    
    @Test
    public void testExponent()
    {
        Postfix p1 = new Postfix("2 3 ^");
        assertEquals(8.0, p1.eval(), 0.001);
        Postfix p2 = new Postfix("2 3 4 ^ *");
        assertEquals(162.0, p2.eval(), 0.001);
        Postfix p3 = new Postfix("2 3 4 * ^");
        assertEquals(4096.0, p3.eval(), 0.001);
    }
    
    @Test
    public void testSqrt()
    {
        Postfix p1 = new Postfix("16 sqrt");
        assertEquals(4.0, p1.eval(), 0.001);
        Postfix p2 = new Postfix("2 sqrt");
        assertEquals(1.4142, p2.eval(), 0.001);
    }
    
    @Test
    public void testCombo()
    {
        Postfix p1 = new Postfix("16 17 - 1 +");
        assertEquals(0.0, p1.eval(), 0.001);
        Postfix p2 = new Postfix("20 5 3 - *");
        assertEquals(40.0, p2.eval(), 0.001);
        Postfix p3 = new Postfix("1 2 3 4 * + -");
        assertEquals(-13, p3.eval(), 0.001);
        Postfix p4 = new Postfix("10 2 ^ 4 / sqrt");
        assertEquals(5, p4.eval(), 0.001);
    }
}
