import java.util.Stack;
import java.util.LinkedList;
public class SY
{
    private String infix;
    
    public SY(String expr)
    {
        infix = (expr.contains(" ")) ? expr : addSpaces(expr);
    }
    
    private String addSpaces(String expr)
    {
        String withSpaces = "";
        String temp = "";
        String [] tokens = expr.split("");
        
        for(String token : tokens)
        {
            if (token.equals("+") || token.equals("-") || token.equals("/") || token.equals("*") || token.equals("^") ||
                token.equals("!") || token.equals("%") || token.equals("(") || token.equals(")") || token.equals("|"))
            {
                if(temp.equals(""))
                {
                    withSpaces = (withSpaces.equals("")) ? token : withSpaces + " " + token;
                }
                else
                {
                    withSpaces = (withSpaces.equals("")) ? temp + " " + token : withSpaces + " " + temp + " " + token;
                    temp = "";
                }
            }
            else
            {
                temp += token;
                if (temp.equals("sin") || temp.equals("cos") || temp.equals("tan") || temp.equals("log") || temp.equals("ln") ||
                    temp.equals("sqrt"))
                {
                    withSpaces = (withSpaces.equals("")) ? temp : withSpaces + temp;
                    temp = "";
                }
            }
        }
        
        if (! temp.equals(""))
                withSpaces = (withSpaces.equals("")) ? temp: withSpaces + " " + temp;
        
        return withSpaces;
    }
    
    private int precedence(String op)
    {
        // functions = 4
        // ^ sqrt ! = 3
        // * / % = 2
        // + - = 1
        switch(op)
        {
            case "+":
            case "-":
                return 1;
            case "*":
            case "/":
            case "%":
                return 2;
            case "^":
            case "sqrt":
            case "!":
                return 3;
            case "sin":
            case "cos":
            case "tan":
            case "log":
            case "ln":
                return 4;
            default:
                return 0;
        }
    }
    public String toPostfix()
    {
        // A stack of strings for the operators
        Stack<String> stack = new Stack<String>();
        // A list of strings for output
        LinkedList<String> postfix = new LinkedList<String>();
        // An array of Strings for tokens
        String[] tokens = infix.split(" ");
        boolean abs = false;
        
        if(tokens.length == 1)
            return tokens[0];
        
        for (String token : tokens)
        {
            if (precedence(token) > 0)
            {
                if(precedence(token) == 4)
                {
                    stack.push(token);
                }
                else if(stack.empty())
                {
                    stack.push(token);
                }
                else
                {
                    while(!stack.empty() && precedence(stack.peek()) >= precedence(token))
                    {
                        postfix.add(stack.pop());
                    }
                    stack.push(token);
                }
            }
            
            else if(token.equals("|"))
            {
                if(!abs)
                {
                    stack.push(token);
                    postfix.add(token);
                }
                else
                {
                    while(! stack.peek().equals("|"))
                    {
                        postfix.add(stack.pop());
                    }
                    postfix.add(stack.pop());
                }
                
                abs = !abs;
            }
            
            else if(token.equals("("))
            {
                stack.push(token);
            }
            
            else if(token.equals(")"))
            {
                while(! stack.peek().equals("("))
                {
                    postfix.add(stack.pop());
                }
                stack.pop();
            }
            
            else // it's hopefully a number
            {
                postfix.add(token);
            }
        }
        
        while(! stack.empty())
        {
            postfix.add(stack.pop());
        }
        
        return String.join(" ", postfix);
    }
}