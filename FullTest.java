

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class FullTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class FullTest
{
    /**
     * Default constructor for test class FullTest
     */
    public FullTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }
    
    @Test
    public void test1()
    {
        SY a = new SY("sqrt(99/11)");
        Postfix b = new Postfix(a.toPostfix());
        assertEquals(3.0, b.eval(), 0.01);
    }
    
    @Test
    public void test2()
    {
        SY a = new SY("|1-3|+2");
        Postfix b = new Postfix(a.toPostfix());
        assertEquals(4, b.eval(), 0.01);
    }
    
    @Test
    public void test3()
    {
        SY a = new SY("log 10 + 4");
        Postfix b = new Postfix(a.toPostfix());
        assertEquals(5,b.eval(),0.01);
    }
    
    @Test
    public void test4()
    {
        SY a = new SY("(3+4)*5");
        Postfix b = new Postfix(a.toPostfix());
        assertEquals(35,b.eval(),0.01);
    }
    
    @Test
    public void test5()
    {
        SY a = new SY("(3+4)*5%6");
        Postfix b = new Postfix(a.toPostfix());
        assertEquals(5,b.eval(),0.01);
    }
}
