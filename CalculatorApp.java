import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;
import javafx.scene.paint.Color;

public class CalculatorApp extends Application
{
    private Label errorLabel;
    private TextField infixField;
    private TextField postfixField;
    private TextField resultField;
    private boolean stored = false;
    private double storedNum;
    
    public void start(Stage window)
    {
        window.setTitle("Calculator");
        window.show();
        
        Pane canvas = new Pane();
        Scene scene = new Scene(canvas, 300, 440);
        window.setScene(scene);
        
        Label infixLabel = new Label("Infix");
        Label postfixLabel = new Label("Postfix");
        errorLabel = new Label("Invalid input");
        Label resultLabel = new Label("Result");
        
        errorLabel.setVisible(false);
        errorLabel.setTextFill(Color.RED);
        
        infixField = new TextField();
        postfixField = new TextField();
        resultField = new TextField();
        
        Button goButton = new Button("Calculate");
        Button clearButton = new Button("Clear");
        Button ansButton = new Button("ANS");
        Button stoRcl = new Button("STO/RCL");
        
        Button zeroButton = new Button("0");
        Button oneButton = new Button("1");
        Button twoButton = new Button("2");
        Button threeButton = new Button("3");
        Button fourButton = new Button("4");
        Button fiveButton = new Button("5");
        Button sixButton = new Button("6");
        Button sevenButton = new Button("7");
        Button eightButton = new Button("8");
        Button nineButton = new Button("9");
        Button addButton = new Button("+");
        Button subButton = new Button("-");
        Button leftButton = new Button("(");
        Button rightButton = new Button(")");
        Button absButton = new Button("|");
        Button multButton = new Button("*");
        Button divButton = new Button("/");
        Button modButton = new Button("%");
        Button sqrtButton = new Button("sqrt");
        Button expButton = new Button("^");
        Button sinButton = new Button("sin");
        Button cosButton = new Button("cos");
        Button tanButton = new Button("tan");
        Button logButton = new Button("log");
        Button lnButton = new Button("ln");
        Button facButton = new Button("!");
        Button decButton = new Button(".");
        
        infixLabel.relocate(20,23);
        postfixLabel.relocate(20,63);
        resultLabel.relocate(20,103);
        infixField.relocate(70,20);
        postfixField.relocate(70,60);
        resultField.relocate(70,100);
        
        goButton.relocate(70,140);
        clearButton.relocate(150,140);
        ansButton.relocate(70, 170);
        stoRcl.relocate(150, 170);
        
        errorLabel.relocate(20,200);
        
        sevenButton.relocate(40, 220);
        fourButton.relocate(40, 250);
        oneButton.relocate(40,280);
        addButton.relocate(40, 310);
        multButton.relocate(40, 340);
        
        eightButton.relocate(90, 220);
        fiveButton.relocate(90, 250);
        twoButton.relocate(90,280);
        zeroButton.relocate(90, 310);
        divButton.relocate(90,340);
        decButton.relocate(90,370);
        
        nineButton.relocate(140, 220);
        sixButton.relocate(140, 250);
        threeButton.relocate(140,280);
        subButton.relocate(140, 310);
        modButton.relocate(140,340);
        
        expButton.relocate(190,220);
        sqrtButton.relocate(190,250);
        sinButton.relocate(190,280);
        cosButton.relocate(190,310);
        tanButton.relocate(190,340);
        
        logButton.relocate(240,220);
        lnButton.relocate(240,250);
        facButton.relocate(240,280);
        leftButton.relocate(240,310);
        rightButton.relocate(240,340);
        absButton.relocate(240, 370);
        
        canvas.getChildren().addAll(postfixLabel, infixLabel, postfixField, infixField, resultLabel,
                                    resultField, goButton, clearButton, errorLabel, oneButton, twoButton,
                                    threeButton, fourButton, fiveButton, sixButton, sevenButton, eightButton,
                                    nineButton, zeroButton, addButton, subButton, leftButton, rightButton,
                                    absButton, multButton, divButton, modButton, sqrtButton, expButton, sinButton,
                                    cosButton, tanButton, logButton, lnButton, facButton, ansButton, stoRcl, decButton);
        
        goButton.setOnAction(this::handleGoButton);
        clearButton.setOnAction(this::handleClearButton);
        ansButton.setOnAction(this::handleAnsButton);
        stoRcl.setOnAction(this::handleStoreRecall);
        
        addButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + "+");
        });
        
        decButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + ".");
        });
        
        subButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + "-");
        });
        
        oneButton.setOnAction((event) -> {
            infixField.setText(infixField.getText() + 1);
        });
        
        twoButton.setOnAction((event) -> {
            infixField.setText(infixField.getText() + 2);
        });
        
        threeButton.setOnAction((event) -> {
            infixField.setText(infixField.getText() + 3);
        });
        
        fourButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + 4);
        });
        
        fiveButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + 5);
        });
        
        sixButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + 6);
        });
        
        sevenButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + 7);
        });
        
        eightButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + 8);
        });
        
        nineButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + 9);
        });
        
        zeroButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + 0);
        });
        
        leftButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + "(");
        });
        
        rightButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + ")");
        });
        
        absButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + "|");
        });
        
        multButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + "*");
        });
        
        divButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + "/");
        });
        
        modButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + "%");
        });
        
        sqrtButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + "sqrt(");
        });
        
        expButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + "^");
        });
        
        sinButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + "sin(");
        });
        
        cosButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + "cos(");
        });
        
        tanButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + "tan(");
        });
        
        logButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + "log(");
        });
        
        lnButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + "ln(");
        });
        
        facButton.setOnAction(event -> {
            infixField.setText(infixField.getText() + "!");
        });
    }
    
    public void handleGoButton(ActionEvent e)
    {
        SY ifx = new SY(infixField.getText());
        try
        {
            String pf = ifx.toPostfix();
            Postfix post = new Postfix(pf);
            Double result = post.eval();
        
            postfixField.setText(pf);
            resultField.setText("" + result);
            errorLabel.setVisible(false);
        }
        catch(NumberFormatException meanString)
        {
            errorLabel.setVisible(true);
        }
        catch(java.util.EmptyStackException badMath)
        {
            errorLabel.setVisible(true);
        }
    }
    
    public void handleClearButton(ActionEvent e)
    {
        infixField.clear();
        postfixField.clear();
        resultField.clear();
        errorLabel.setVisible(false);
    }
    
    public void handleAnsButton(ActionEvent e)
    {
        infixField.setText(resultField.getText());
        postfixField.clear();
        resultField.clear();        
    }
    
    public void handleStoreRecall(ActionEvent e)
    {
        if (stored)
        {
            infixField.setText(infixField.getText() + storedNum);
        }
        else
        {
            storedNum = Double.parseDouble(resultField.getText());
        }
        stored = !stored;
    }
}